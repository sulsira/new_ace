@include('__partials/doc')
@yield('header')
@yield('jumbotron')
		<div class="container">
			<div class="row">
	        	@yield('content')
	        </div><!-- end the content aread -->				
		</div><!-- end of container -->
@yield('footer')
