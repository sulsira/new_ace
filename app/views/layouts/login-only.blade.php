@include('__partials/doc')
	<div class="wrapper">
		<div class="sidea"></div><!-- sidea -->
		<div class="content">
	    <div class="container-fluid">
	      <div class="row">
	      	@yield('sidebar')
	        <div class="col-sm-9 col-sm-offset-3 col-md-10 col-md-offset-2 main"> <!-- the content aread -->
	        	@yield('content')
	        </div><!-- end the content aread -->
	      </div>
	    </div>
			
		</div><!-- end of content -->
		<div class="sideb"></div><!-- sideb -->
	</div><!-- end of wrapper -->	
@yield('footer')
