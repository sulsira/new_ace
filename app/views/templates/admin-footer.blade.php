@section('footer')

   		<footer>
      <div class="container">
        <p>© Footer <?php echo date('Y') ?></p>
      </div>
  			
  		</footer>
    <!-- Bootstrap core JavaScript
    ================================================== -->
    <!-- Placed at the end of the document so the pages load faster -->
    {{HTML::script('bootstrap/js/jquery.min.js')}}
    {{HTML::script('bootstrap/js/bootstrap.min.js')}}
    {{HTML::script('bootstrap/js/docs.min.js')}}
    {{HTML::script('bootstrap/js/ie10-viewport-bug-workaround.js')}}
    <!-- IE10 viewport hack for Surface/desktop Windows 8 bug -->
</body>
</html>
@stop